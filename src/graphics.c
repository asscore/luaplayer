#include <stdio.h>

#include "graphics.h"

#define RATIO 1.33

static GSGLOBAL *gsGlobal = NULL;
static GSTEXTURE gsTexture;

static const u64 BLACK_RGBAQ = GS_SETREG_RGBAQ(0x00,0x00,0x00,0x00,0x00);
static const u64 TEXTURE_RGBAQ = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x80,0x00);

static float x_center;
static float y_center;
static float w_ratio;
static float h_ratio;
static int texture_size;

void init_graphics(void)
{
	/* initialize the GS */
	gsGlobal = gsKit_init_global();

	/* initialize the DMAC */
	dmaKit_init(D_CTRL_RELE_OFF, D_CTRL_MFD_OFF, D_CTRL_STS_UNSPEC, D_CTRL_STD_OFF, D_CTRL_RCYC_8, 1 << DMA_CHANNEL_GIF);
	dmaKit_chan_init(DMA_CHANNEL_GIF);

	gsGlobal->ZBuffering = GS_SETTING_OFF;
	gsGlobal->PSM = GS_PSM_CT32;

	gsKit_init_screen(gsGlobal);

	gsKit_mode_switch(gsGlobal, GS_ONESHOT);

	gsTexture.Width = PS2_LINE_SIZE;
	gsTexture.Height = SCREEN_HEIGHT;
	gsTexture.PSM = GS_PSM_CT32;
	gsTexture.Filter = GS_FILTER_LINEAR;

	texture_size = gsKit_texture_size(gsTexture.Width , gsTexture.Height, GS_PSM_CT32);

	gsTexture.Mem = (u32 *)memalign(128, texture_size);
	gsTexture.Vram = gsKit_vram_alloc(gsGlobal, texture_size, GSKIT_ALLOC_USERBUFFER);

	memset((void *)gsTexture.Mem, '\0', texture_size);

	w_ratio = gsTexture.Width * RATIO;
	h_ratio = gsTexture.Height * RATIO;

	x_center = (gsGlobal->Width  - w_ratio) / 2;
	y_center = (gsGlobal->Height - h_ratio) / 2;

	gsKit_clear(gsGlobal, BLACK_RGBAQ);
	gsKit_queue_exec(gsGlobal);
	gsKit_sync_flip(gsGlobal);
}

Color *get_vram_draw_buffer()
{
	return gsTexture.Mem;
}

void clear_screen(Color color)
{
	int i;
	Color* data = get_vram_draw_buffer();
	for (i = (texture_size / 4); i--;)
		*data++ = color;
}

void flip_screen()
{
	gsKit_clear(gsGlobal, BLACK_RGBAQ);

	/* upload new frame buffer */
	gsKit_texture_upload(gsGlobal, &gsTexture);

	gsKit_prim_sprite_striped_texture(gsGlobal, &gsTexture,
					x_center, y_center, /* x1,y1 */
					0.0f, 0.0f, /* u1,v1 */
					w_ratio + x_center,
					h_ratio + y_center, /* x2,y2 */
					gsTexture.Width,
					gsTexture.Height, /* u2,v2 */
					1, TEXTURE_RGBAQ);

	/* execute render queue */
	gsKit_queue_exec(gsGlobal);
	/* vsync and flip buffer */
	gsKit_sync_flip(gsGlobal);
}

void put_pixel_screen(Color color, int x, int y)
{
	Color* vram = get_vram_draw_buffer();
	vram[PS2_LINE_SIZE * y + x] = color;
}

Color get_pixel_screen(int x, int y)
{
	Color* vram = get_vram_draw_buffer();
	return vram[PS2_LINE_SIZE * y + x];
}
