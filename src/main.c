#include <kernel.h>
#include <sifrpc.h>
#include <iopcontrol.h>
#include <loadfile.h>
#include <sbv_patches.h>
#include <libmc.h>
#include <string.h>
#include <unistd.h>

#include "luaplayer.h"
#include "graphics.h"
#include "boot.h"

char boot_path[255];

void reset_iop(void)
{
	while (!SifIopReset("", 0));
	while (!SifIopSync());
	SifInitRpc(0);
	SifLoadFileInit();
}

extern u8 iomanX_irx_start[];
extern u32 iomanX_irx_size;
extern u8 fileXio_irx_start[];
extern u32 fileXio_irx_size;
extern u8 usbd_irx_start[];
extern u32 usbd_irx_size;
extern u8 usbhdfsd_irx_start[];
extern u32 usbhdfsd_irx_size;

void load_modules(void)
{
	int ret;

	SifLoadModule("rom0:SIO2MAN", 0, NULL);
	SifLoadModule("rom0:MCMAN", 0, NULL);
	SifLoadModule("rom0:MCSERV", 0, NULL);
	SifLoadModule("rom0:PADMAN", 0, NULL);

	SifExecModuleBuffer(iomanX_irx_start, iomanX_irx_size, 0, NULL, &ret);
	SifExecModuleBuffer(fileXio_irx_start, fileXio_irx_size, 0, NULL, &ret);
	SifExecModuleBuffer(usbd_irx_start, usbd_irx_size, 0, NULL, &ret);
	SifExecModuleBuffer(usbhdfsd_irx_start, usbhdfsd_irx_size, 0, NULL, &ret);

	sleep(1);
}

static inline size_t chr_idx(const char *s, char c)
{
	size_t i = 0;
	while (s[i] && (s[i] != c)) i++;
	return (s[i] == c) ? i : -1;
}

static inline size_t last_chr_idx(const char *s, char c)
{
	size_t i = strlen(s);
	while (i && s[--i] != c);
	return (s[i] == c) ? i : -1;
}

static int __dirname(char *filename)
{
	int i;
	if (filename == NULL) return -1;
	i = last_chr_idx(filename, '/');
	if (i < 0) {
		i = last_chr_idx(filename, '\\');
		if (i < 0) {
			i = chr_idx(filename, ':');
			if (i < 0)
				return -2;
		}
	}
	filename[i+1] = '\0';
	return 0;
}

int system_main(int argc, char **argv)
{
	SifInitRpc(0);

	if (argc != 0)
		reset_iop();

	sbv_patch_enable_lmb();
	sbv_patch_disable_prefix_check();

	load_modules();

	mcInit(MC_TYPE_MC);

	if (argc == 0) {
		strcpy(boot_path, "host:");
	} else {
		strcpy(boot_path, argv[0]);
		__dirname(boot_path);
	}

	return 0;
}

int lua_main(int argc, char **argv)
{
	const char *err_msg;
	int c = 0;
	char *s;

	init_graphics();

	chdir(boot_path);

	s = boot_path;
	while (*s) if (*s++ == ':') ++c;
	if (c > 1) { /* PCSX2 ? - host:<drive>:... */
		err_msg = run_script("app\\index.lua", 1);
	} else {
		/* execute Lua script (according to boot sequence) */
		char *script = (char *)malloc(size_boot_string + 1);
		memcpy(script, boot_string, size_boot_string);
		boot_string[size_boot_string] = 0;
		err_msg = run_script(script, 0);
		free(script);
	}

	if (err_msg != NULL)
		printf("Error: %s\n", err_msg);

	return 0;
}

int main(int argc, char *argv[])
{
	system_main(argc, argv);
	lua_main(argc, argv);

	SleepThread();

	return 0;
}
