#include <string.h>

#include "luaplayer.h"

static lua_State *L;

int unlink = 0; /* undefined reference to `unlink' */
int link = 0; /* undefined reference to `link' */

const char *run_script(const char* script, int is_file)
{
	L = luaL_newstate();

	luaL_openlibs(L);

	lua_system_init(L);
	lua_graphics_init(L);

	int s = 0;
	const char *err_msg = NULL;

	if (is_file)
		s = luaL_loadfile(L, script);
	else
		s = luaL_loadbuffer(L, script, strlen(script), NULL);


	if (s == 0)
		s = lua_pcall(L, 0, LUA_MULTRET, 0);

	if (s) {
		err_msg = lua_tostring(L, -1);
		lua_pop(L, 1);
	}

	lua_close(L);

	return err_msg;
}
