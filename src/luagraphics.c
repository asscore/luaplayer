#include "luaplayer.h"
#include "graphics.h"

UserdataStubs(Color, Color)

static int lua_flip(lua_State *L)
{
	flip_screen();
	return 0;
}

static int lua_clear(lua_State *L) {
	int argc = lua_gettop(L);
	if (argc != 0 && argc != 1)
		return luaL_error(L, "Argument error: screen.clear([color]) zero or one argument.");
	Color color = (argc == 1) ? *toColor(L, 1) : 0;
	clear_screen(color);
	return 0;
}

static int lua_pixel(lua_State *L) {
	int argc = lua_gettop(L);
	if (argc != 2 && argc != 3)
		return luaL_error(L, "screen.pixel(x, y, [color]) takes two or three arguments, and must be called with a colon.");
	int x = luaL_checkint(L, 1);
	int y = luaL_checkint(L, 2);
	Color color = (argc == 3) ? *toColor(L, 3) : 0;
	if (x >= 0 && y >= 0 && x < SCREEN_WIDTH && y < SCREEN_HEIGHT) {
		if (argc==2) {
			*pushColor(L) = get_pixel_screen(x, y);
			return 1;
		} else {
			put_pixel_screen(color, x, y);
			return 0;
		}
	}

	return luaL_error(L, "An argument was incorrect.");
}

static int Color_new(lua_State *L) {
	int argc = lua_gettop(L);
	if (argc != 3 && argc != 4)
		return luaL_error(L, "Argument error: Color.new(r, g, b, [a]) takes either three color arguments or three color arguments and an alpha value.");

	Color *color = pushColor(L);

	unsigned r = CLAMP(luaL_checkint(L, 1), 0, 255);
	unsigned g = CLAMP(luaL_checkint(L, 2), 0, 255);
	unsigned b = CLAMP(luaL_checkint(L, 3), 0, 255);
	unsigned a;
	if (argc == 4)
		a = CLAMP(luaL_checkint(L, 4), 0, 255);
	else
		a = 255;

	*color = a << 24 | b << 16 | g << 8 | r;

	return 1;
}
static int Color_colors(lua_State *L) {
	int argc = lua_gettop(L);
	if(argc != 1)
		return luaL_error(L, "Argument error: color:colors() takes no arguments, and it must be called from an instance with a colon.");
	Color color = *toColor(L, 1);
	int r = R(color);
	int g = G(color);
	int b = B(color);
	int a = A(color);

	lua_newtable(L);
	lua_pushstring(L, "r"); lua_pushnumber(L, r); lua_settable(L, -3);
	lua_pushstring(L, "g"); lua_pushnumber(L, g); lua_settable(L, -3);
	lua_pushstring(L, "b"); lua_pushnumber(L, b); lua_settable(L, -3);
	lua_pushstring(L, "a"); lua_pushnumber(L, a); lua_settable(L, -3);

	return 1;
}

static int Color_tostring(lua_State *L) {
	Color_colors(L);
	lua_pushstring(L, "r"); lua_gettable(L, -2); int r = luaL_checkint(L, -1); lua_pop(L, 1);
	lua_pushstring(L, "g"); lua_gettable(L, -2); int g = luaL_checkint(L, -1); lua_pop(L, 1);
	lua_pushstring(L, "b"); lua_gettable(L, -2); int b = luaL_checkint(L, -1); lua_pop(L, 1);
	lua_pushstring(L, "a"); lua_gettable(L, -2); int a = luaL_checkint(L, -1); lua_pop(L, 1);
	lua_pop(L, 1); /* pop the table */
	lua_pushfstring(L, "Color (r %d, g %d, b %d, a %d)", r, g, b, a);
	return 1;
}

static int Color_equal(lua_State *L) {
	Color a = *toColor(L, 1);
	Color b = *toColor(L, 2);
	lua_pushboolean(L, a == b);
	return 1;
}
static const luaL_reg Color_methods[] = {
	{"new", Color_new},
	{"colors", Color_colors},
	{0,0}
};
static const luaL_reg Color_meta[] = {
	{"__tostring", Color_tostring},
	{"__eq", Color_equal},
	{0,0}
};

UserdataRegister(Color, Color_methods, Color_meta)

static const luaL_reg screen_functions[] = {
	{"flip", lua_flip},
	{"clear", lua_clear},
	{"pixel", lua_pixel},
	{0,0}
};

void lua_graphics_init(lua_State *L)
{
	Color_register(L);

	luaL_openlib(L, "screen", screen_functions, 0);
}
