#ifndef __GRAPHICS_H
#define __GRAPHICS_H

#include <gsKit.h>
#include <dmaKit.h>

#define	PS2_LINE_SIZE 480
#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 272

typedef u32 Color;

#define A(color) ((u8)(color >> 24 & 0xFF))
#define B(color) ((u8)(color >> 16 & 0xFF))
#define G(color) ((u8)(color >> 8 & 0xFF))
#define R(color) ((u8)(color & 0xFF))

void init_graphics(void);

Color* get_vram_draw_buffer(void);
void clear_screen(Color color);
void flip_screen(void);
void put_pixel_screen(Color color, int x, int y);
Color get_pixel_screen(int x, int y);

#endif /* __GRAPHICS_H */
