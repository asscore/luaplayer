#include <fileXio_rpc.h>
#include <string.h>
#include <unistd.h>

#include "luaplayer.h"

extern char boot_path[255];

char *__ps2_normalize_path(char *path_name);

static int lua_getcwd(lua_State *L)
{
	char path[256];
	getcwd(path, 256);
	lua_pushstring(L, path);

	return 1;
}

static int lua_chdir(lua_State *L)
{
	static char temp_path[256];
	const char *path = luaL_checkstring(L, 1);
	if (!path)
		return luaL_error(L, "Argument error: System.currentDirectory(file) takes a filename as string as argument.");

	lua_getcwd(L);

	/* if absolute path (contains [drive]:path/) */
	if (strchr(path, ':')) {
		strcpy(temp_path, path);
	} else { /* relative path */
		if (!strncmp(path, "..", 2)) { /* remove last directory ? */
			getcwd(temp_path, 256);
			if ((temp_path[strlen(temp_path) - 1] != ':')) {
				int idx = strlen(temp_path) - 1;
				do {
					idx--;
				} while (temp_path[idx] != '/');
				temp_path[idx] = '\0';
			}
		} else { /* add given directory to the existing path */
			getcwd(temp_path, 256);
			strcat(temp_path, "/");
			strcat(temp_path, path);
		}
	}

	chdir(__ps2_normalize_path(temp_path));

	return 1;
}

static int lua_curdir(lua_State *L) {
	int argc = lua_gettop(L);
	if (argc == 0) return lua_getcwd(L);
	if (argc == 1) return lua_chdir(L);
	return luaL_error(L, "Argument error: System.currentDirectory([file]) takes zero or one argument.");
}

static int lua_dir(lua_State *L)
{
	int argc = lua_gettop(L);
	if (argc != 0 && argc != 1)
		return luaL_error(L, "Argument error: System.listDirectory([path]) takes zero or one argument.");

	const char *temp_path = "";
	char path[255];

	getcwd((char *)path, 256);

	if (argc != 0) {
		temp_path = luaL_checkstring(L, 1);
		/* append the given path to the boot_path */
		strcpy((char *)path, boot_path);

		/* workaround in case of temp_path is containing a device name again */
		if (strchr(temp_path, ':'))
			strcpy((char *)path, temp_path);
		else
			strcat((char *)path, temp_path);
	}

	strcpy(path,__ps2_normalize_path(path));

	int fd = fileXioDopen((char *)path);
	if (fd < 0) {
		lua_pushnil(L);
		return 1;
	}

	lua_newtable(L);

	int i = 1;
	iox_dirent_t dircontent;

	while (fileXioDread(fd, &dircontent) > 0) {
		lua_pushnumber(L, i++);

		lua_newtable(L);

		lua_pushstring(L, "name");
		lua_pushstring(L, dircontent.name);
		lua_settable(L, -3);

		lua_pushstring(L, "size");
		lua_pushnumber(L, dircontent.stat.size);
		lua_settable(L, -3);

		lua_pushstring(L, "directory");
		lua_pushboolean(L, FIO_S_ISDIR(dircontent.stat.mode));
		lua_settable(L, -3);

		lua_settable(L, -3);
	}

	fileXioDclose(fd);

	return 1;
}

static const luaL_reg System_functions[] = {
	{"currentDirectory", lua_curdir},
	{"listDirectory", lua_dir},
	{0, 0}
};

void lua_system_init(lua_State *L) {
	luaL_openlib(L, "System", System_functions, 0);
}
