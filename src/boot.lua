flist = System.listDirectory()
dofiles = { 0, 0, 0 }

for idx, file in ipairs(flist) do
	if file.name ~= "." and file.name ~= ".." then
		if string.lower(file.name) == "script.lua" then -- luaplayer/script.lua
			dofiles[1] = file.name
		end
		if file.directory then
			fflist = System.listDirectory(file.name)
			for fidx, ffile in ipairs(fflist) do
				if string.lower(ffile.name) == "script.lua" then -- app bundle
					dofiles[2] = file.name.."/"..ffile.name
				end
				if string.lower(ffile.name) == "index.lua" then -- app bundle
					dofiles[2] = file.name.."/"..ffile.name
				end
			end
		end
	end
end
done = false
for idx, runfile in ipairs(dofiles) do
	if runfile ~= 0 then
		dofile(runfile)
		done = true
		break
	end
end

if not done then
	print("Boot error: No boot script found.")
end
