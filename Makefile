EE_BIN = luaplayer.elf
EE_BIN_PACKED = luaplayer-packed.elf

EE_INCS += -I$(PS2SDK)/ports/include
EE_INCS += -I$(GSKIT)/include

EE_LDFLAGS += -L$(PS2SDK)/ports/lib
EE_LDFLAGS += -L$(GSKIT)/lib

EE_LIBS += -lg -lpatches
EE_LIBS += -lfileXio
EE_LIBS += -lmc
EE_LIBS += -llua -llualib
EE_LIBS += -lgskit -ldmakit

EE_OBJS += src/main.o
EE_OBJS += src/luaplayer.o
EE_OBJS += src/luasystem.o
EE_OBJS += src/graphics.o
EE_OBJS += src/luagraphics.o
EE_OBJS += src/iomanX_irx.o
EE_OBJS += src/fileXio_irx.o
EE_OBJS += src/usbd_irx.o
EE_OBJS += src/usbhdfsd_irx.o

all: $(EE_BIN)
	$(EE_STRIP) $(EE_BIN)
	ps2-packer $(EE_BIN) $(EE_BIN_PACKED)

clean:
	rm -f $(EE_BIN) $(EE_BIN_PACKED) $(EE_OBJS)
	rm -f src/boot.h

run:
	ps2client reset; sleep 3
	ps2client execee host:$(EE_BIN)

# Embedded resources
src/main.o: src/boot.h

src/boot.h: src/boot.lua
	bin2c $< $@ boot_string

# Embedded Irx(s)
src/iomanX_irx.o: $(PS2SDK)/iop/irx/iomanX.irx
	bin2o $< $@ iomanX_irx

src/fileXio_irx.o: $(PS2SDK)/iop/irx/fileXio.irx
	bin2o $< $@ fileXio_irx

src/usbd_irx.o: $(PS2SDK)/iop/irx/usbd.irx
	bin2o $< $@ usbd_irx

src/usbhdfsd_irx.o: $(PS2SDK)/iop/irx/usbhdfsd.irx
	bin2o $< $@ usbhdfsd_irx


include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal
